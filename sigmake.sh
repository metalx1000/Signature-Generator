#!/bin/bash

if [ $# -lt 1 ]
then
  echo "Text Needed."
  echo "Example: $0 'John Hancock'"
  exit 1
fi

sig="$1"
mkdir "$sig"

find fonts -iname "*.ttf"|while read i
do
  f="$(basename "$i"|cut -d\. -f1)"
  echo "Font $f..."
  convert -size 800x100 xc:none -font "$i" -pointsize 50 -style italic -draw "gravity NorthWest text 20,10 '$sig" "$sig/$f.png"
done

#export -f createsig
#find fonts -iname "*.ttf" -exec bash -c 'createsig "$0"' {} \;

xdg-open "$sig"
